package ru.alishev.springcourse.LibraryProject.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.alishev.springcourse.LibraryProject.models.Book;
import ru.alishev.springcourse.LibraryProject.models.Person;
import ru.alishev.springcourse.LibraryProject.repositories.BooksRepository;

import java.util.List;
import java.util.Optional;


@Service
@Transactional(readOnly = true)
public class BooksService {

    private BooksRepository booksRepository;

    @Autowired
    public BooksService(BooksRepository booksRepository) {
        this.booksRepository = booksRepository;
    }

    public List<Book> findAll() {
        return booksRepository.findAll();
    }

    public Page<Book> getBooksWithPagination(Pageable pageable) {
        return booksRepository.findAll(pageable);
    }

    public Book findOne(int id) {
        Optional<Book> foundBook = booksRepository.findById(id);
        return foundBook.orElse(null);
    }

    public List<Book> findBooksByBookName(String name) {
        return booksRepository.findByBookNameContaining(name);
    }

    @Transactional
    public void save(Book book) {
        booksRepository.save(book);
    }

    @Transactional
    public void update(int id, Book updatedBook) {
        Book bookToBeUpdated = booksRepository.findById(id).orElse(null);
        if (bookToBeUpdated != null) {
            updatedBook.setOwner(bookToBeUpdated.getOwner());
            updatedBook.setId(id);
            booksRepository.save(updatedBook);
        }
    }

    @Transactional
    public void setOwnerNull(int id) {
        Book book = booksRepository.findById(id).orElse(null);
        if (book != null) {
            Person owner = book.getOwner();
            book.setOwner(null);
            book.setRentTime(null);
            owner.getListOfBooks().remove(book);
        }
    }

    @Transactional
    public void delete(int id) {
        booksRepository.deleteById(id);
    }
}
