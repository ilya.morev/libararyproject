package ru.alishev.springcourse.LibraryProject.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.alishev.springcourse.LibraryProject.models.Book;
import ru.alishev.springcourse.LibraryProject.models.Person;
import ru.alishev.springcourse.LibraryProject.repositories.PeopleRepository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Service
@Transactional(readOnly = true)
public class PeopleService {

    private PeopleRepository peopleRepository;

    @Autowired
    public PeopleService(PeopleRepository peopleRepository) {
        this.peopleRepository = peopleRepository;
    }

    public List<Person> findAll() {
        return peopleRepository.findAll();
    }

    public Person findOne(int id) {
        Optional<Person> foundPerson = peopleRepository.findById(id);
        return foundPerson.orElse(null);
    }

    @Transactional
    public void save(Person person) {
        peopleRepository.save(person);
    }

    @Transactional
    public void update(int id, Person updatedPerson) {
        updatedPerson.setId(id);
        peopleRepository.save(updatedPerson);
    }

    @Transactional
    public void delete(int id) {
        peopleRepository.deleteById(id);
    }

    public List<Boolean> checkRentTime(List<Boolean> rentCheckList, Person owner) {
        List<Book> bookList = owner.getListOfBooks();
        for (Book book : bookList){
            Timestamp timestamp = book.getRentTime();
            Date currentTime = new Date();

            long timeDifference = (currentTime.getTime() - timestamp.getTime());
            long daysDifference = TimeUnit.MILLISECONDS.toDays(timeDifference);

            rentCheckList.add(daysDifference >= 10);
        }

        return rentCheckList;
    }

    public Person nameCheck(String name){
        return peopleRepository.findByName(name);
    }
}
