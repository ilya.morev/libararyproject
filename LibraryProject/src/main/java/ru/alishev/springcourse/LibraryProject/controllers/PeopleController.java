package ru.alishev.springcourse.LibraryProject.controllers;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.*;
import ru.alishev.springcourse.LibraryProject.models.Book;
import ru.alishev.springcourse.LibraryProject.models.Person;
import ru.alishev.springcourse.LibraryProject.services.PeopleService;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/people")
public class PeopleController {
    private PeopleService peopleService;
    private Validator personValidator;

    @Autowired
    public PeopleController(PeopleService peopleService, Validator validator) {
        this.peopleService = peopleService;
        this.personValidator = validator;
    }

    @GetMapping()
    public String index(Model model) {
        model.addAttribute("people", peopleService.findAll());
        return "people/index";
    }

    @GetMapping("/{id}")
    public String show(Model model, @PathVariable("id") int id) {
        Person person = peopleService.findOne(id);
        List<Boolean> rentCheckedList = new ArrayList<>();

        model.addAttribute("person", person);
        model.addAttribute("rentedBooks", person.getListOfBooks());
        model.addAttribute("checkedRent", peopleService.checkRentTime(rentCheckedList, person)); // ДОЛЖНО РАБОТАТЬ,
        // НУЖНО ПРОВЕРИТЬ В ПРЕДСТАВЛЕНИИ, КОД ЕСТЬ У ГПТ
        return "people/show";
    }

    @GetMapping("/new")
    public String newPerson(@ModelAttribute("person") Person person) {
        return "people/new";
    }

    @PostMapping()
    public String create(@ModelAttribute("person") @Valid Person person,
                         BindingResult bindingResult) {

        if (bindingResult.hasErrors())
            return "people/new";
        peopleService.save(person);
        return "redirect:/people";
    }

    @GetMapping("/{id}/edit")
    public String edit(Model model, @PathVariable("id") int id) {
        model.addAttribute("person", peopleService.findOne(id));
        return "people/edit";
    }

    @PatchMapping("/{id}")
    public String update(@ModelAttribute("person") @Valid Person updatedPerson, BindingResult bindingResult,
                         @PathVariable("id") int id) {

        if (bindingResult.hasErrors())
            return "people/edit";

        peopleService.update(id, updatedPerson);
        return "redirect:/people";
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") int id) {
        peopleService.delete(id);
        return "redirect:/people";
    }
}