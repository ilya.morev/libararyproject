package ru.alishev.springcourse.LibraryProject.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.alishev.springcourse.LibraryProject.models.Person;
import ru.alishev.springcourse.LibraryProject.services.PeopleService;

@Component
public class PersonValidator implements Validator {
    private PeopleService peopleService;

    @Autowired
    public PersonValidator(PeopleService peopleService) {
        this.peopleService = peopleService;
    }


    @Override
    public boolean supports(Class<?> clazz) {
        return Person.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Person person = (Person) target;
        System.out.println("VALIDATION");
        System.out.println(person.getName());
        if (peopleService.nameCheck(person.getName()) != null){
            errors.rejectValue("name", "", "Имя должно быть уникальным");
        }

    }
}
