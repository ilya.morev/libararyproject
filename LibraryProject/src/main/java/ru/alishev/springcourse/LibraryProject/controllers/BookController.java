package ru.alishev.springcourse.LibraryProject.controllers;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.alishev.springcourse.LibraryProject.models.Book;
import ru.alishev.springcourse.LibraryProject.models.Person;
import ru.alishev.springcourse.LibraryProject.services.BooksService;
import ru.alishev.springcourse.LibraryProject.services.PeopleService;

import java.sql.Timestamp;
import java.util.List;


@Controller
@RequestMapping("/books")
public class BookController {

    private BooksService booksService;
    private PeopleService peopleService;

    @Autowired
    public BookController(BooksService booksService, PeopleService peopleService) {
        this.booksService = booksService;
        this.peopleService = peopleService;
    }

    @GetMapping()
    public String index(Model model,
                        @RequestParam(required = false, defaultValue = "0") int page,
                        @RequestParam(required = false, defaultValue = "100") int size,
                        @RequestParam(name = "sort_by_year", required = false, defaultValue = "false") boolean sortByYear,
                        Pageable pageable) {

        if (sortByYear){
            pageable = PageRequest.of(page,size,Sort.by("year"));
        }
        else pageable = PageRequest.of(page, size);

        Page<Book> pageOfBooks = booksService.getBooksWithPagination(pageable);
        model.addAttribute("books", pageOfBooks);
        return "books/index";
    }

    @GetMapping("/{id}")
    public String show(Model model, @PathVariable("id") int id, @ModelAttribute("bookOwner") Person person) {

        model.addAttribute("book", booksService.findOne(id));
        model.addAttribute("personList", peopleService.findAll());
        return "books/show";
    }

    @GetMapping("/search")
    public String search(@RequestParam(name="query", required = false) String query, Model model){

        if (query == null)
            return "books/search";

        List<Book> books = booksService.findBooksByBookName(query);

        if (books.isEmpty()){
            model.addAttribute("message", "Такой книги не найдено");
        }
        else {
            model.addAttribute("books", books);
        }

        return "books/search-results";
    }

    @PatchMapping("/addOwner")
    public String addOwner(@ModelAttribute("bookOwner") Person person, @RequestParam("bookId") int bookId) {
        Book rentedBook = booksService.findOne(bookId);
        Person newOwner = peopleService.findOne(person.getId());
        rentedBook.setOwner(newOwner);
        booksService.update(bookId, rentedBook);
        return "redirect:/books/" + bookId;
    }

    @PatchMapping("/deleteOwner")
    public String deleteOwner(@RequestParam("bookId") int bookId) {
        booksService.setOwnerNull(bookId);
        return "redirect:/books/" + bookId;
    }

    @GetMapping("/new")
    public String newBook(@ModelAttribute("book") Book book) {
        return "books/new";
    }

    @PostMapping()
    public String create(@ModelAttribute("book") @Valid Book book, BindingResult bindingResult) {

        if (bindingResult.hasErrors())
            return "books/new";

        booksService.save(book);
        return "redirect:/books";
    }

    @GetMapping("/{id}/edit")
    public String edit(Model model, @PathVariable("id") int id) {
        model.addAttribute("book", booksService.findOne(id));
        return "books/edit";
    }

    @PatchMapping("/{id}")
    public String update(@ModelAttribute("book") @Valid Book updatedBook, BindingResult bindingResult,
                         @PathVariable("id") int id) {

        if (bindingResult.hasErrors())
            return "books/edit";

        booksService.update(id, updatedBook);
        return "redirect:/books";
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") int id) {
        booksService.delete(id);
        return "redirect:/books";
    }
}