package ru.alishev.springcourse.LibraryProject.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "person")
public class Person {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Имя не должно быть пустым")
    @Size(min = 5, max = 40, message = "ФИО должно быть в промежутке от 5 до 40 символов")
    @Column(name = "name")
    private String name;
    @NotNull(message = "У человека должен быть возраст")
    @Min(value = 0, message = "Возраст не может быть меньше 0 лет")
    @Column(name = "age")
    private int age;
    @OneToMany(mappedBy = "owner", cascade = CascadeType.PERSIST)
    private List<Book> listOfBooks = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Person() {
    }

    public List<Book> getListOfBooks() {
        return listOfBooks;
    }

    public void setListOfBooks(List<Book> listOfBooks) {
        this.listOfBooks = listOfBooks;
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", listOfBooks=" + listOfBooks +
                '}';
    }
}
