package ru.alishev.springcourse.LibraryProject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.alishev.springcourse.LibraryProject.models.Book;

import java.util.List;

public interface BooksRepository extends JpaRepository<Book, Integer> {
    public List<Book> findByBookNameContaining(String search);
}
