package ru.alishev.springcourse.LibraryProject.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;

import java.sql.Timestamp;


@Entity
@Table(name = "Book")
public class Book {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Название книги не должно быть пустым")
    @Size(min = 2, max = 40, message = "Нозвание книги должно быть в промежутке от 2 до 40 символов")
    @Column(name = "book_name")
    private String bookName;
    @NotNull(message = "У книги должен быть автор")
    @Size(min = 2, max = 50, message = "Имя автора должно быть в промежутке от 2 до 50 символов")
    @Column(name = "author")
    private String author;

    @ManyToOne
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    private Person owner;

    @NotNull(message = "У книги должен быть год выпуска")
    @Min(value = 1200, message = "Год выпуска не может быть меньше 1200 года")
    @Max(value = 2023, message = "Год выпуска не может быть больше 2023 года")
    @Column(name = "year")
    private int year;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "rent_time")
    private Timestamp rentTime;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getRentTime() {
        return rentTime;
    }

    public void setRentTime(Timestamp rentTime) {
        this.rentTime = rentTime;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
        this.rentTime = new Timestamp(System.currentTimeMillis());
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Book() {
    }

    public Book(String bookName, String author, int year, Person owner) {
        this.bookName = bookName;
        this.author = author;
        this.owner = owner;
        this.year = year;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookName='" + bookName + '\'' +
                ", author='" + author + '\'' +
                ", year=" + year +
                '}';
    }
}
