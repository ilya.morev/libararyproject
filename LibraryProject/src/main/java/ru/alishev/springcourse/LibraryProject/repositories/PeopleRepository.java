package ru.alishev.springcourse.LibraryProject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.alishev.springcourse.LibraryProject.models.Person;

public interface PeopleRepository extends JpaRepository<Person, Integer> {
    public Person findByName(String name);
}
